# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import WebDriverException
import unittest, time, re


class UntitledTestCase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.katalon.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_untitled_test_case(self):
        driver = self.driver
        driver.get("http://vum.bg/hello-vum/")
        time.sleep(5)
        driver.find_element_by_xpath('//*[@id="panel-w5978b385b7435-0-0-1"]/div/div/ul/li[4]/a').click()
        time.sleep(10)
        driver.get_screenshot_as_file('instagram_test.png')
        time.sleep(2)


    def test_element_is_clickable(self):
        driver = self.driver
        driver.get("http://vum.bg/hello-vum/")
        time.sleep(5)
        element = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, "pg-2422-0"))
        )
        try:
            element.click()
        except WebDriverException:
            print
            "Element is not clickable"
        time.sleep(10)
        driver.get_screenshot_as_file('instagram_test.png')
        time.sleep(2)

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
       self.driver.quit()
       self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
